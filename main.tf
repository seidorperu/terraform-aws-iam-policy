resource "aws_iam_policy" "this" {
  name        = "${var.owner}-${var.env}-${var.project}"
  policy      = "${file("${var.policy}")}"
}