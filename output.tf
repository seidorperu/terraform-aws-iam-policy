output "arn" {
  value = "${aws_iam_policy.this.arn}"
}
output "id" {
  value = "${aws_iam_policy.this.id}"
}
output "name" {
  value = "${aws_iam_policy.this.name}"
}
