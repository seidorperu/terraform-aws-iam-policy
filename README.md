# AWS Policy module
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Terraform module which creates Policy on AWS, compatible Terraform >= v0.12.1 .

These types of resources are supported:

* [Policy](https://www.terraform.io/docs/providers/aws/r/iam_policy.html)


## Usage

```hcl
module "aws_iam_policy" {
    source                 = "git::ssh://git@bitbucket.org:seidorperu/terraform-aws-iam-policy.git"
    owner                  = "empresa"
    project                = "lambda"
    env                    = "dev"
    policy                 = "file.json"
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| owner | Propietario del proyecto. | map | n/a | yes |
| project | Nombre del proyecto. | map | n/a | yes |
| env | Entorno de despliegue. | map | n/a | yes |
| policy | Archivo Json- Policy | map | n/a | yes |

## Example Template Policy-Json

```hlc
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DeregisterImage",
                "ec2:RegisterImage",
                "ec2:CreateImage",
                "ec2:DescribeImages",
                "ec2:DescribeImageAttribute",
                "ec2:DeleteTags",
                "ec2:DescribeInstances"
            ],
            "Resource": [
                "*"
            ]
        }
    ]
}
```
## Outputs

| Name | Description |
|------|-------------|
| arn | El ARN asignado por AWS a esta política.. |
| id | El id de la política.. |
| name | NOmbre de la politica. |


## Authors

Seidor 

## License

Apache 2 Licensed. See LICENSE for full details.